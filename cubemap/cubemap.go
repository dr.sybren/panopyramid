package cubemap

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of PanoPyramid.
 *
 * PanoPyramid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PanoPyramid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PanoPyramid.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image"
	"image/color"
	"image/draw"
	"math"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/skyfill/lincolor"
)

func outImgToXYZ(sideX, sideY, face, sideSize int) (x, y, z float64) {
	// get x,y,z coords from out image pixels coords
	// i,j are pixel coords
	// face is face number
	// edge is edge length
	a := 2.0 * float64(sideX) / float64(sideSize)
	b := 2.0 * float64(sideY) / float64(sideSize)
	switch face {
	case 0: // back
		return -1.0, 1.0 - a, 3.0 - b
	case 1: // left
		return a - 3.0, -1.0, 3.0 - b
	case 2: // front
		return 1.0, a - 5.0, 3.0 - b
	case 3: // right
		return 7.0 - a, 1.0, 3.0 - b
	case 4: // top
		return b - 1.0, a - 5.0, 1.0
	case 5: // bottom
		return 5.0 - b, a - 5.0, -1.0
	default:
		logrus.WithField("face", face).Panic("no such face exists")
	}
	panic("unreachable")
}

func clamp(value, min, max int) int {
	if value < min {
		return min
	}
	if value > max {
		return max
	}
	return value
}

// Use bilinear interpolation between the four surrounding pixels
func interpolate(source image.Image, u, v float64) color.Color {
	u1 := int(math.Floor(u))
	v1 := int(math.Floor(v))

	u2 := u1 + 1
	v2 := v1 + 1

	mu := u - float64(u1) // fraction of way across pixel
	nu := v - float64(v1)

	width := source.Bounds().Dx()
	maxY := source.Bounds().Dy() - 1

	// Pixel values of four corners
	colourA := source.At(u1%width, clamp(v1, 0, maxY))
	colourB := source.At(u2%width, clamp(v1, 0, maxY))
	colourC := source.At(u1%width, clamp(v2, 0, maxY))
	colourD := source.At(u2%width, clamp(v2, 0, maxY))

	mixV1 := lincolor.Alpha(colourA, colourB, mu)
	mixV2 := lincolor.Alpha(colourC, colourD, mu)

	return lincolor.Alpha(mixV1, mixV2, nu)
}

// FromEquirectangular converts an equirectangular image to a single cube map image.
func FromEquirectangular(equi image.Image) []*image.NRGBA {
	startTime := time.Now()
	equiWidth := equi.Bounds().Dx()
	sideSize := equiWidth / 4
	logrus.WithField("sideSize", sideSize).Debug("creating cube map")

	cubeBounds := image.Rect(0, 0, sideSize*4-1, sideSize*3-1)
	cubemap := image.NewNRGBA(cubeBounds)

	// var x, y, z float64
	var theta, r, phi float64
	var uf, vf float64
	var iStart, iEnd int
	var face int
	// for face := 0; face < 6; face++ {
	for j := 0; j < cubeBounds.Dy(); j++ {
		if j < sideSize || j >= sideSize*2 {
			iStart = 2 * sideSize
			iEnd = 3 * sideSize
			if j < sideSize {
				face = 4 // top
			} else {
				face = 5 // bottom
			}
		} else {
			iStart = 0
			iEnd = cubeBounds.Dx()
			face = -1
		}

		for i := iStart; i < iEnd; i++ {
			if face != 4 && face != 5 {
				face = i / sideSize // 0 - back, 1 - left 2 - front, 3 - right
			}
			x, y, z := outImgToXYZ(i, j, face, sideSize)

			theta = math.Atan2(y, x) // range -pi to pi
			r = math.Hypot(x, y)
			phi = math.Atan2(z, r) // range -pi/2 to pi/2

			// source img coords
			uf = 2.0 * float64(sideSize) * (theta + math.Pi) / math.Pi
			vf = 2.0 * float64(sideSize) * (math.Pi/2 - phi) / math.Pi

			pixelColour := interpolate(equi, uf, vf)
			cubemap.Set(i, j, pixelColour)
		}
	}

	logrus.WithField("duration", time.Now().Sub(startTime)).Info("cube map created")

	// TODO: instead of allocating the entire cube map image and then copying
	// its contents, just draw directly onto the side images.
	sideBounds := image.Rect(0, 0, sideSize-1, sideSize-1)
	sides := make([]*image.NRGBA, 6)
	for idx := range sides {
		sides[idx] = image.NewNRGBA(sideBounds)
	}
	draw.Draw(sides[0], sideBounds, cubemap, image.Pt(2*sideSize, 0), draw.Src)
	draw.Draw(sides[1], sideBounds, cubemap, image.Pt(0, sideSize), draw.Src)
	draw.Draw(sides[2], sideBounds, cubemap, image.Pt(1*sideSize, sideSize), draw.Src)
	draw.Draw(sides[3], sideBounds, cubemap, image.Pt(2*sideSize, sideSize), draw.Src)
	draw.Draw(sides[4], sideBounds, cubemap, image.Pt(3*sideSize, sideSize), draw.Src)
	draw.Draw(sides[5], sideBounds, cubemap, image.Pt(2*sideSize, 2*sideSize), draw.Src)

	return sides
}
