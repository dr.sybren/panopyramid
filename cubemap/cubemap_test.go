package cubemap

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of PanoPyramid.
 *
 * PanoPyramid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PanoPyramid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PanoPyramid.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClamp(t *testing.T) {
	assert.Equal(t, 1, clamp(1, 1, 1))
	assert.Equal(t, 2, clamp(1, 2, 5))
	assert.Equal(t, 3, clamp(3, 2, 5))
	assert.Equal(t, 4, clamp(4, 2, 5))
	assert.Equal(t, 5, clamp(5, 2, 5))
	assert.Equal(t, 5, clamp(6, 2, 5))
}
