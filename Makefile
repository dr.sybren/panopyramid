OUT := panopyramid
PKG := gitlab.com/dr.sybren/panopyramid
VERSION := $(shell git describe --tags --dirty --always)
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
STATIC_OUT := ${OUT}-v${VERSION}

ifeq ($(OS),Windows_NT)
	OUT := $(OUT).exe
endif

all: application

application:
	go build -i -v -o ${OUT} -ldflags="-X main.applicationVersion=${VERSION}" ${PKG}

install:
	go install -i -v -ldflags="-X main.applicationVersion=${VERSION}" ${PKG}

version:
	@echo "OS     : ${OS}"
	@echo "Package: ${PKG}"
	@echo "Version: ${VERSION}"
	@echo "Target : ${OUT}"

test:
	go test -short ${PKG_LIST}

vet:
	@go vet ${PKG_LIST}

lint:
	@for file in ${GO_FILES} ;  do \
		golint $$file ; \
	done

run: application
	./${OUT}

clean:
	@go clean -i -x
	rm -f ${OUT}-v*

static: vet lint
	go build -i -v -o ${STATIC_OUT} -tags netgo -ldflags="-extldflags \"-static\" -w -s -X main.applicationVersion=${VERSION}" ${PKG}

.PHONY: run application version static vet lint
