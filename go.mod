module gitlab.com/dr.sybren/panopyramid

go 1.13

require (
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	gitlab.com/dr.sybren/skyfill v0.0.0-20190912141550-a59fe54b2714
	golang.org/x/image v0.0.0-20190227222117-0694c2d4d067
)
