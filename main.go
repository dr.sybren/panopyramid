package main

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of PanoPyramid.
 *
 * PanoPyramid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PanoPyramid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PanoPyramid.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/panopyramid/cubemap"
	"gitlab.com/dr.sybren/panopyramid/imgio"
	"gitlab.com/dr.sybren/skyfill/lincolor"
)

const applicationName = "PanoPyramid"

var applicationVersion = "set-during-build"

var cliArgs struct {
	filename string
	quiet    bool
	debug    bool
}

var linearModel = lincolor.LinearModel

func parseCliArgs() {
	flag.BoolVar(&cliArgs.quiet, "quiet", false, "Disable info-level logging.")
	flag.BoolVar(&cliArgs.debug, "debug", false, "Enable debug-level logging.")
	flag.Parse()

	if flag.NArg() != 1 {
		flag.Usage()
		fmt.Println("  filename")
		fmt.Println("        Input file; JPEG, PNG, or TIFF file in 8- or 16-bit RGB or RGBA mode")
		os.Exit(1)
	}

	cliArgs.filename = flag.Arg(0)
	if cliArgs.filename == "" {
		logrus.Fatal("empty filename not allowed")
	}
}

func configLogging() {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	// Only log the warning severity or above by default.
	var level logrus.Level
	switch {
	case cliArgs.debug:
		level = logrus.DebugLevel
	case cliArgs.quiet:
		level = logrus.WarnLevel
	default:
		level = logrus.InfoLevel
	}
	logrus.SetLevel(level)
	log.SetOutput(logrus.StandardLogger().Writer())
}

func makeOutPrefix(inputFilename string) string {
	outPath := path.Clean(cliArgs.filename)
	outExt := filepath.Ext(outPath)
	outPrefix := outPath[0 : len(outPath)-len(outExt)]
	return outPrefix
}

func main() {
	parseCliArgs()
	configLogging()
	logrus.WithField("version", applicationVersion).Infof("starting %s", applicationName)

	pathPrefix := makeOutPrefix(cliArgs.filename)

	m := imgio.LoadImage(cliArgs.filename)
	sides := cubemap.FromEquirectangular(m)

	for idx, side := range sides {
		fname := pathPrefix + fmt.Sprintf("-%d.jpg", idx)
		logrus.WithField("filename", fname).Info("writing cube side")
		imgio.WriteImage(fname, side)
	}

	logrus.Info("done")
}
