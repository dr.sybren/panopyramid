package imgio

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of PanoPyramid.
 *
 * PanoPyramid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PanoPyramid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PanoPyramid.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image"
	"image/jpeg"
	"image/png"
	"os"
	"path"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

// WriteImage writes to a PNG file.
func WriteImage(fname string, img image.Image) {
	ext := path.Ext(fname)

	logger := logrus.WithField("file", fname)
	logger.Debug("writing image")
	startTime := time.Now()

	writer, err := os.Create(fname)
	if err != nil {
		logger.WithError(err).Fatal("unable to open file")
	}
	defer writer.Close()

	switch strings.ToLower(ext) {
	case ".png":
		err = png.Encode(writer, img)
	case ".jpg", ".jpeg":
		err = jpeg.Encode(writer, img, &jpeg.Options{Quality: 90})
	default:
		logger.WithField("ext", ext).Fatal("unknown extension, unable to write image")
	}
	if err != nil {
		logger.WithError(err).Fatal("unable to write image")
	}

	duration := time.Now().Sub(startTime)
	logger.WithField("writeDuration", duration).Info("image written")
}
